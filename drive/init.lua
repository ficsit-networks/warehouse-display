
function print_r(table)
    for i, v in pairs(table) do
        print(i,v)
    end
end


-- SCREEN_SIZE = {140, 20}
SCREEN_SIZE = {100, 15}
--SCREEN_SIZE = {160, 24}
--SCREEN_SIZE = {160, 20}
-- SCREEN_SIZE = {320, 100}
-- SCREEN_SIZE = {130, 32}
-- SCREEN_SIZE = {131, 30}

NR_COLUMNS = 3
COL_WIDTHS = {nil, 5}
PAGE_DURATION = 5
local COLORS <const> = {
    { {1,1,1,1}, {0,0,0,0} },
    { {1,1,1,1}, {0.1,0.1,0.1,0.5} }
}
USE_EXTERNAL_SCREEN = true

function init(colors)
    screen = Screen:new()

    containers = component.proxy(component.findComponent(class['FGBuildableStorage']))
    print("Found " .. #containers .. " containers")

    inventory = WarehouseInventory:new(containers)
    page = Page:new(screen, inventory, colors)

    return page
end


-- Reimplementation of table.sort as a workaround
-- for C stack overflow errors
table.oldSort = table.sort
function table:sort(cmpFunc, startIndex, lastIndex)
    -- Init
    if cmpFunc == nil then
        table.oldSort(self)
        return
        -- cmpFunc = function(a,b)
        --     return a < b
        -- end
    end

    if startIndex == nil then
        startIndex = 1
    end

    if lastIndex == nil then
        lastIndex = #self
    end

    table._sort(self, cmpFunc, startIndex, lastIndex)
end
function table:_sort(cmpFunc, startIndex, lastIndex)
    wall = startIndex

    if lastIndex - startIndex < 1 then
        -- Nothing to sort
        return
    end

    -- Moving median value to the back to avoid bad performance when sorting an already sorted array
    table.switchValues(self, math.ceil( (startIndex + lastIndex) / 2), lastIndex)

    -- Splitting the array according to comparisons with the last value
    for i = startIndex, lastIndex, 1 do
        if cmpFunc(self[i], self[lastIndex]) then
            table.switchValues(self, i, wall)
            wall = wall + 1
        end
    end

    -- Placing last value between the two split arrays
    table.switchValues(self, wall, lastIndex)

    -- Sorting left of the wall
    table.sort(self, cmpFunc, startIndex, wall - 1)

    -- Sorting right of the wall
    table.sort(self, cmpFunc, wall + 1, lastIndex)
end
function table:switchValues(firstIndex, secondIndex)
    if firstIndex ~= secondIndex then
        self[firstIndex], self[secondIndex] = self[secondIndex], self[firstIndex]
    end
end


-- String helper function
function string.truncate(str, length)
    if string.len(str) <= length then
        return str
    end

    return string.sub(str, 1, length)
end


-- Actual warehouse inventory display code
Screen = {
    gpu = nil,
    width = 0,
    height = 0
}
function Screen:new()
    o = {}
    setmetatable(o, self)
    self.__index = self

    -- get first T1 GPU avialable from PCI-Interface
    self.gpu = computer.getPCIDevices(classes["GPUT1"])[1]
    if not self.gpu then
        error("No GPU T1 found!")
        computer.stop()
    end

    if (not USE_EXTERNAL_SCREEN) then
        -- get first Screen-Driver available from PCI-Interface
        screen = computer.getPCIDevices(classes["FINComputerScreen"])[1]
    end

    -- if no screen found, try to find large screen from component network
    if not screen then
        comp = component.findComponent(classes["Screen"])[1]
        -- comp = component.findComponent("primaryScreen")
        if not comp then
            error("No Screen found!")
            computer.stop()
        end
        screen = component.proxy(comp)
    end

    -- setup gpu
    self.gpu:bindScreen(screen)
    self.gpu:setSize(table.unpack(SCREEN_SIZE))
    self.width, self.height = self.gpu:getSize()

    return o
end
function Screen:clear()
    self.gpu:setBackground(0,0,0,0)
    self.gpu:fill(0,0, self.width,self.height, " ")
    self.gpu:flush()
end
function Screen:flush()
    self.screen:flush()
end


WarehouseInventory = {
    containers = {},
    items = {}
}
function WarehouseInventory:new(containers)
   o = {}
   setmetatable(o, self)
   self.__index = self

   self.containers = containers

   return o
end
function WarehouseInventory:update()
    self:wipe()

    for _, container in pairs(self.containers) do
        inventories  = container:getInventories()
        inventory = inventories[1]

        for j = 0, inventory.size - 1 do
            stack = inventory:getStack(j)
            if stack == nil then
                goto continue
            end

            item = stack.item
            if item.type == nil then
                goto continue
            end

            self:addItems(stack.count, item.type)

            ::continue::
        end
    end
end
function WarehouseInventory:addItems(count, itemType)
    item = self.items[itemType.internalName] or {}
    item["name"] = itemType.name
    item["count"] = count + (item.count or 0)

    self.items[itemType.internalName] = item
end
function WarehouseInventory:wipe()
    self.items = {}
end
function WarehouseInventory:sort()
    -- Turn items table into array
    sortableItems = {}
    for _, item in pairs(self.items) do
        table.insert(sortableItems, item)
    end

    table.sort(sortableItems, function(a, b) return a.name < b.name end)
    self.items = sortableItems
end


Page = {
    screen = nil,
    inventory = {},
    colors = {},

    lastColumn = NR_COLUMNS - 1,
    lastPage = 0,
    gutterWidth = 1,
    colPos = {},
    colWidths = {},
    maxNrRows = 1,
    currNrRows = 1,
}
function Page:new(screen, inventory, colors)
    o = {}
    setmetatable(o, self)
    self.__index = self

    self.screen = screen
    self.inventory = inventory
    self.colors = colors

    self:init()

    return o
end
function Page:init()
    self.countStringLength = 4
    self.maxNrRows = self.screen.height
    self.itemIndex = 1

    self.colWidths = self:getColWidths()
    self.colPos = self:getColPos(self.colWidths)
end
function Page:getColPos(colWidths)
    colPos = {0}
    colPosCounter = 0

    for i = 1, #colWidths - 1, 1 do
        colPosCounter = colPosCounter + colWidths[i]
        if i % 2 == 0 then
            colPosCounter = colPosCounter + self.gutterWidth
        end

        table.insert(colPos, colPosCounter)
    end

    return colPos
end
function Page:getColWidths()
    colWidth = math.floor( (self.screen.width - (NR_COLUMNS - 1) * self.gutterWidth) / NR_COLUMNS )
    rest = self.screen.width - (NR_COLUMNS * colWidth) - (NR_COLUMNS-1) * self.gutterWidth

    countColWidth = self.countStringLength + 1 -- leave one space free on the right
    textColWidth = colWidth - countColWidth

    colWidths = {}
    for i = 1, NR_COLUMNS - 1, 1 do
        table.insert(colWidths, textColWidth)
        table.insert(colWidths, countColWidth)
    end

    table.insert(colWidths, textColWidth+rest)
    table.insert(colWidths, countColWidth)

    return colWidths
end
function Page:updateInventory()
    self.inventory:update()
    self.inventory:sort()

    self.lastPage = math.ceil(#self.inventory.items / (NR_COLUMNS * self.maxNrRows)) - 1
    self.currNrRows = math.min(#self.inventory.items/NR_COLUMNS, self.maxNrRows)
end
function Page:display(pageNr)
    self.screen:clear()

    if pageNr > self.lastPage then
        local message = "Cannot display page " .. pageNr .. " because there only are " .. self.lastPage + 1 .. " pages"
        print(message)
        self.screen.gpu:setText(1, 1, message)
        self.screen.gpu:flush()
        return
    end

    for colNr = 0, self.lastColumn, 1 do
        self:printColumn(pageNr, colNr)
    end
end
function Page:printColumn(pageNr, colNr)
    if colNr == 0 then
        if pageNr == self.lastPage then
            -- Last page; spread items evenly across columns
            nrItemsLeft = 1 + #self.inventory.items - self.itemIndex
            self.currNrRows = math.ceil(nrItemsLeft / NR_COLUMNS)
        else
            self.currNrRows = self.maxNrRows
        end
    end

    y = 0
    last = self.itemIndex + self.currNrRows - 1
    colPosIndex = 1 + 2*colNr

    while self.itemIndex <= last do
        item = self.inventory.items[self.itemIndex]
        if item == nil then
            goto display
        end

        self.screen.gpu:setForeGround(table.unpack(self.colors[1 + y % 2][1]))
        self.screen.gpu:setBackGround(table.unpack(self.colors[1 + y % 2][2]))
        self.screen.gpu:fill(
            self.colPos[colPosIndex],
            y,
            self.colWidths[colPosIndex] + self.colWidths[colPosIndex+1],
            1,
            " "
        )

        self.screen.gpu:setText(
            1 + self.colPos[colPosIndex],
            y,
            string.truncate(item.name, self.colWidths[colPosIndex] - 2)
        )

        count = item.count
        self.screen.gpu:setText(
            self.colPos[colPosIndex+1] + self.colWidths[colPosIndex+1] - string.len(count) - 1,
            y,
            count
        )

        y = y + 1
        self.itemIndex = self.itemIndex + 1
    end

    ::display::
    self.screen.gpu:flush()

    if self.itemIndex > #self.inventory.items and colNr == NR_COLUMNS - 1 then
        self.itemIndex = 1
    end
end

page = init(COLORS)

i = 0
while true do
    if i > page.lastPage then
        -- We can't use a for-loop because page.lastPage may change during the loop
        -- as a result of the page.updateInventory() call below
        i = 0
    end

    page:updateInventory()
    page:display(i)
    event.pull(PAGE_DURATION)

    i = i + 1
end
